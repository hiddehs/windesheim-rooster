#!/usr/bin/env bash
echo "Deploying windesheimrooster.nl back- & front-end"
mkdir -p ~/$CI_COMMIT_REF_NAME/ && cd ~/$CI_COMMIT_REF_NAME/
if cd windesheim-rooster; then git checkout $CI_COMMIT_REF_NAME && git pull; else git clone --single-branch --branch $BITBUCKET_BRANCH git@gitlab.com:hiddehs/windesheim-rooster.git && cd windesheim-rooster; fi
docker-compose up -d  --force-recreate --build
