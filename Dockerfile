FROM node:10-alpine

WORKDIR /usr/src/app


# Install app dependencies

COPY package*.json .
COPY yarn.lock .

RUN yarn install

# Bundle app source
COPY . .

ARG NODE_ENV=production
RUN echo ${NODE_ENV}
# Build yarn
RUN yarn build

# Set environment variables
ENV NUXT_HOST 0.0.0.0
ENV NUXT_PORT 3020

EXPOSE 3020
CMD [ "yarn", "start" ]
